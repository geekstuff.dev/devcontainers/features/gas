# Geekstuff.dev / Devcontainers / Features / Gas

This devcontainer feature installs Gas cli.

https://github.com/leighmcculloch/gas

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/gas": {}
    }
}
```

You can use a `debian`, `ubuntu` or `alpine` image as the base.

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/gas/-/tags).
